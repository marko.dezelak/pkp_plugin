**Prerequisite**
Download/clone and start: https://gitlab.com/mKuznii/tehnicen-dolg

**SET UP**
1) Download/clone PKP_plugin and PKP_app

**PKP_app**

 2) Open app.config and add your: email, apiEndpoint
 3) Build project

**PKP_plugin**

 3) Open TehnicniDolg.cs and add PKP_app.exe to Execute method as shown in example
 4) Run solution